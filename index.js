//console.log("hello");

let posts = [];
let count = 1;

// Add post data
	//addEventListener(event, callback function that will be triggered if the event occur or happen)
	document.querySelector("#form-add-post").addEventListener("submit", (event)=> {
		//prevent Default function - stops the auto reload of the webpage when submitting
		event.preventDefault();

		posts.push({
			id: count,
			title: document.querySelector("#txt-title").value,
			body: document.querySelector("#txt-body").value
		})
		//console.log(posts)

		count++;
		showPosts(posts)
	})

// Show posts

	const showPosts = (posts) =>{
		let postEntries = '';

		posts.forEach((post) =>{
			postEntries += `<div id = "post-${post.id}">
			<h3 id = "post-title-${post.id}">${post.title}</h3>
			<p id = "post-body-${post.id}">${post.body}</p>
			<button onclick = "editPost('${post.id}')">Edit</button>
			<button onclick = "deletePost('${post.id}')">Delete</button>
			</div>`
		})

		//console.log(postEntries);


		let postBody = document.querySelector('#div-post-entries');

		postBody.innerHTML = postEntries;

	}

//Edit post

	const editPost = (id) => {
		let title = document.querySelector(`#post-title-${id}`).innerHTML;
		let body = document.querySelector(`#post-body-${id}`).innerHTML;

		document.querySelector('#txt-edit-title').value = title;
		document.querySelector('#txt-edit-body').value = body;
		document.querySelector('#txt-edit-id').value = id;
	}

document.querySelector("#form-edit-post").addEventListener("submit", (event)=>{
	event.preventDefault();

	const idToBeEdited = document.querySelector('#txt-edit-id').value;
	
	for(let i = 0; i < posts.length; i++){
		if(posts[i].id.toString() === idToBeEdited){
			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].body = document.querySelector('#txt-edit-body').value;
			showPosts(posts);
			alert('Edit is Successful');
			break;
		}
	}

})


const deletePost = (id1) => {
	let numberId = +id1;
	//console.log(typeof(numberId));

	let postIdToBeDelete = posts.findIndex(indexId => {
		return (indexId.id === numberId);
	});
	console.log(postIdToBeDelete)

	//posts.splice(postIdToBeDelete, 1);
	posts.splice(postIdToBeDelete, 1);
	console.log(posts);
	let deletedDOM = document.querySelector(`#post-${id1}`);
	deletedDOM.remove();

}